# Hardware.PM-03.03

## Placa Madre

Autor: 

- Agustín Gonzalez

Revisión: 

- Fernando Marotta
- German Vazquez

Hardware:

- Conexión Ethernet
- Conexión con Modulo de Audio
- 2 RAM 512Kbit
- Fuente 3.3V

Software:

- Eagle 6.5.0 Light

Usos:

- Semáforo: Ethernet, RAM, Fuente, Audio
- Consola: Ethernet, Fuente, LCD
- Comandos: Ethernet, SPI, Fuente, LCD